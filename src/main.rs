extern crate rand;
extern crate image;
extern crate num_traits;

use image::{ImageBuffer, Rgb};
use num_traits::clamp;
use std::ops::{Sub, Mul, Div, Add, Neg};
use rand::prelude::random;

#[derive(Debug, Copy, Clone)]
struct Vector3 {
    x: f32,
    y: f32,
    z: f32,
}

#[derive(Debug, Copy, Clone)]
struct Vector2 {
    x: f32,
    y: f32,
}

#[derive(Debug, Copy, Clone)]
struct Size {
    x: u32,
    y: u32,
}

#[derive(Debug, Copy, Clone)]
struct Point3(Vector3);

#[derive(Debug, Copy, Clone)]
struct NormalVector3(Vector3);

#[derive(Debug, Copy, Clone)]
struct Colour(Vector3);

#[derive(Debug, Copy, Clone)]
struct Ray {
    origin: Point3,
    direction: NormalVector3,
}

struct Sphere {
    centre: Point3,
    radius: f32,
}

struct Plane {
    point: Point3,
    normal: NormalVector3,
}

struct UniformLight {
    centre: Point3,
    colour: Colour,
    sampler: Sampler,
    radius: f32,
}

impl Neg for Vector3 {
    type Output = Vector3;

    fn neg(self) -> Vector3 {
        Vector3::zero() - self
    }
}

impl Neg for NormalVector3 {
    type Output = NormalVector3;

    fn neg(self) -> NormalVector3 {
        NormalVector3(-self.0)
    }
}

impl Sub for Vector3 {
    type Output = Vector3;

    fn sub(self, other: Vector3) -> Vector3 {
        Vector3::new(self.x - other.x, self.y - other.y, self.z - other.z)
    }
}

impl Mul<f32> for Vector3 {
    type Output = Vector3;

    fn mul(self, scalar: f32) -> Vector3 {
        Vector3::new(self.x * scalar, self.y * scalar, self.z * scalar)
    }
}

impl Div<f32> for Vector3 {
    type Output = Vector3;

    fn div(self, scalar: f32) -> Vector3 {
        Vector3::new(self.x / scalar, self.y / scalar, self.z / scalar)
    }
}

impl Mul for Vector3 {
    type Output = Vector3;

    fn mul(self, other: Vector3) -> Vector3 {
        Vector3::new(self.x * other.x, self.y * other.y, self.z * other.z)
    }
}

impl Mul<f32> for Vector2 {
    type Output = Vector2;

    fn mul(self, scalar: f32) -> Vector2 {
        Vector2::new(self.x * scalar, self.y * scalar)
    }
}


impl Add for Vector3 {
    type Output = Vector3;

    fn add(self, other: Vector3) -> Vector3 {
        Vector3::new(self.x + other.x, self.y + other.y, self.z + other.z)
    }
}

impl Add for Colour {
    type Output = Colour;

    fn add(self, other: Colour) -> Colour {
        Colour(self.0 + other.0)
    }
}

impl Mul<f32> for Colour {
    type Output = Colour;

    fn mul(self, scalar: f32) -> Colour {
        Colour(self.0 * scalar)
    }
}

impl Div<f32> for Colour {
    type Output = Colour;

    fn div(self, scalar: f32) -> Colour {
        Colour(self.0 / scalar)
    }
}

impl Mul for Colour {
    type Output = Colour;

    fn mul(self, other: Colour) -> Colour {
        Colour(self.0 * other.0)
    }
}

impl Add<Vector3> for Point3 {
    type Output = Point3;

    fn add(self, other: Vector3) -> Point3 {
        Point3(self.0 + other)
    }
}

impl<'a> Mul<Vector3> for &'a OrthonormalBasis {
    type Output = Vector3;

    fn mul(self, other: Vector3) -> Vector3 {
        self.u.0 * other.x + self.v.0 * other.y + self.w.0 * other.z
    }
}

impl UniformLight {
    fn point(position: Point3, colour: Colour) -> UniformLight {
        UniformLight {
            centre: position,
            colour: colour,
            sampler: Sampler::zero(),
            radius: 0.0,
        }
    }

    fn soft(position: Point3, colour: Colour, radius: f32, sample_row: u32) -> UniformLight {
        UniformLight {
            centre: position,
            colour: colour,
            sampler: Sampler::generate(1, &|| SampleSet2::regular(sample_row)),
            radius: radius,
        }
    }

    fn sample(&self) -> Point3 {
        self.centre + remap_to_unit_sphere(self.sampler.sample() * self.radius)
    }
}

impl Size {
    fn new(x: u32, y: u32) -> Size {
        Size { x: x, y: y }
    }
}

impl Vector2 {
    fn new(x: f32, y: f32) -> Vector2 {
        Vector2 { x: x, y: y }
    }

    fn zero() -> Vector2 {
        Vector2::new(0.0, 0.0)
    }
}

impl Vector3 {
    fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x: x, y: y, z: z }
    }

    fn from_polar(r: f32, t: f32, z: f32) -> Vector3 {
        Vector3::new(r * t.cos(), r * t.sin(), z)
    }

    fn zero() -> Vector3 {
        Vector3::new(0.0, 0.0, 0.0)
    }

    fn length_sq(self) -> f32 {
        self.dot(self)
    }

    fn length(self) -> f32 {
        self.length_sq().sqrt()
    }

    fn direction(from: Point3, to: Point3) -> Vector3 {
        to.0 - from.0
    }

    fn distance(from: Point3, to: Point3) -> f32 {
        Vector3::direction(from, to).length()
    }

    fn dot(self, other: Vector3) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    fn normalise(self) -> NormalVector3 {
        let len = self.length();
        NormalVector3(Vector3::new(self.x / len, self.y / len, self.z / len))
    }

    fn cross(a: Vector3, b: Vector3) -> Vector3 {
        Vector3 {
            x: a.y * b.z - a.z * b.y,
            y: a.z * b.x - a.x * b.z,
            z: a.x * b.y - a.y * b.x,
        }
    }

    fn reflect(self, normal: NormalVector3) -> Vector3 {
        let dot = normal.0.dot(self);
        normal.0 * dot * 2.0 - self
    }
}

impl Ray {
    fn new(origin: Point3, direction: NormalVector3) -> Ray {
        Ray {
            origin: origin,
            direction: direction,
        }
    }

    fn from_to(start: Point3, point: Point3) -> Ray {
        Ray {
            origin: start,
            direction: NormalVector3::direction(start, point),
        }
    }

    fn follow(self, t: f32) -> Point3 {
        return self.origin + (self.direction.0 * t);
    }

    const EPSILON: f32 = 0.000001;
}

impl Point3 {
    fn new(x: f32, y: f32, z: f32) -> Point3 {
        Point3(Vector3::new(x, y, z))
    }
}

impl NormalVector3 {
    fn direction(from: Point3, to: Point3) -> NormalVector3 {
        Vector3::direction(from, to).normalise()
    }

    fn reflect(self, normal: NormalVector3) -> NormalVector3 {
        let dot = normal.dot(self);
        NormalVector3(normal.0 * dot * 2.0 - self.0)
    }

    fn dot(self, other: NormalVector3) -> f32 {
        self.0.dot(other.0)
    }
}

impl Colour {
    fn rgb(x: f32, y: f32, z: f32) -> Colour {
        Colour(Vector3::new(x, y, z))
    }

    fn black() -> Colour {
        Colour::rgb(0.0, 0.0, 0.0)
    }

    fn white() -> Colour {
        Colour::rgb(1.0, 1.0, 1.0)
    }
}

struct SampleSet2 {
    samples: Vec<Vector2>,
}

impl SampleSet2 {
    fn zero() -> SampleSet2 {
        SampleSet2 { samples: vec![Vector2::zero()] }
    }

    fn regular(row_size: u32) -> SampleSet2 {
        let mut result = vec![Vector2::zero(); (row_size * row_size) as usize];
        for x in 0..row_size {
            for y in 0..row_size {
                let frac_x = (x as f32 + 0.5) / (row_size as f32);
                let frac_y = (y as f32 + 0.5) / (row_size as f32);
                result[(x * row_size + y) as usize] = Vector2::new(frac_x, frac_y);
            }
        }
        SampleSet2 { samples: result }
    }
}

struct Sampler {
    sets: Vec<SampleSet2>,
}

impl Sampler {
    fn zero() -> Sampler {
        Sampler { sets: vec![SampleSet2::zero()] }
    }

    fn generate(set_count: u32, generate: &(Fn() -> SampleSet2)) -> Sampler {
        let mut sets = Vec::new();
        for i in 0..set_count {
            sets.push(generate());
        }
        Sampler { sets: sets }
    }

    fn sample(&self) -> Vector2 {
        let set = &self.sets[0].samples;
        let rand_val: usize = random();
        set[rand_val % set.len()]
    }
}

fn remap_to_unit_sphere(sample: Vector2) -> Vector3 {
    let z = 2.0 * sample.x - 1.0;
    let t = 2.0 * std::f32::consts::PI * sample.y;
    let r = (1.0 - z * z).sqrt();
    Vector3::from_polar(r, t, z)
}

impl Shape for Sphere {
    fn intersect(&self, ray: Ray) -> Option<IntersectionInfo> {
        let distance = Vector3::direction(self.centre, ray.origin);
        let a = 1.0; // ray.direction.length_sq()
        let b = (distance * 2.0).dot(ray.direction.0);
        let c = distance.length_sq() - self.radius * self.radius;
        let delta = b * b - 4.0 * a * c;
        if delta >= 0.0 {
            let delta_root = delta.sqrt();
            let t = f32::min((-b - delta_root) / (2.0 * a), (-b + delta_root) / (2.0 * a));
            let hit_point = ray.follow(t);
            let normal = NormalVector3::direction(self.centre, hit_point);
            Some(IntersectionInfo {
                distance: t,
                normal: normal,
            })
        } else {
            None
        }
    }
}

impl Shape for Plane {
    fn intersect(&self, ray: Ray) -> Option<IntersectionInfo> {
        Some(IntersectionInfo {
            distance: Vector3::direction(ray.origin, self.point).dot(self.normal.0) /
                ray.direction.dot(self.normal),
            normal: self.normal,
        })
    }
}

struct IntersectionInfo {
    distance: f32,
    normal: NormalVector3,
}

struct HitInfo {
    ray: Ray,
    point: Point3,
    normal: NormalVector3,
    depth: i32,
}

impl HitInfo {
    fn new(ray: Ray, intersection: IntersectionInfo, depth: i32) -> HitInfo {
        HitInfo {
            ray: ray,
            point: ray.follow(intersection.distance),
            normal: intersection.normal,
            depth: depth,
        }
    }
}

struct OrthonormalBasis {
    u: NormalVector3,
    v: NormalVector3,
    w: NormalVector3,
}

impl OrthonormalBasis {
    fn new(front: Vector3, up: Vector3) -> OrthonormalBasis {
        let w = front.normalise();
        let u = Vector3::cross(up, w.0).normalise();
        let v = Vector3::cross(w.0, u.0).normalise();
        OrthonormalBasis { u: u, v: v, w: w }
    }
}

trait Material {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour;
}

trait Shape {
    fn intersect(&self, ray: Ray) -> Option<IntersectionInfo>;
}

struct AmbientMaterial {
    ambient: Colour,
}

struct DiffuseMaterial {
    ambient: Colour,
    diffuse: Colour,
}

struct PhongMaterial {
    ambient: Colour,
    diffuse: Colour,
    specular: Colour,
    exponent: f32,
}

struct ReflectiveMaterial {
    phong: PhongMaterial,
    reflection_colour: Colour,
}

struct TransparentMaterial {
    phong: PhongMaterial,
    reflection_colour: Colour,
    transmission_colour: Colour,
    refraction: f32,
}


impl Material for AmbientMaterial {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour {
        self.ambient
    }
}

impl Material for DiffuseMaterial {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour {
        let mut total = self.ambient;
        for light in scene.lights.iter() {
            total = total + self.shade_light(hit, light, scene);
        }
        total
    }
}

impl Material for ReflectiveMaterial {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour {
        let mut total = self.phong.ambient;
        for light in scene.lights.iter() {
            total = total + self.shade_light(hit, light, scene);
        }
        total
    }
}

impl Material for TransparentMaterial {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour {
        let mut total = self.phong.ambient;
        for light in scene.lights.iter() {
            total = total + self.shade_light(hit, light, scene);
        }
        total
    }
}

impl TransparentMaterial {
    fn shade_light(&self, hit: &HitInfo, light: &UniformLight, scene: &Scene) -> Colour {
        let luminance = self.phong.shade_light(hit, light, scene);

        let to_camera = -hit.ray.direction;
        let cos_incident_angle = hit.normal.dot(to_camera);
        let eta = if cos_incident_angle > 0.0 {
            self.refraction
        } else {
            1.0 / self.refraction
        };
        let refraction_coeff = find_refraction_coeff(eta, cos_incident_angle);
        let reflected_ray = Ray::new(hit.point, to_camera.reflect(hit.normal));
        let reflected_colour = scene.trace_ray(reflected_ray, hit.depth);

        let reflected = if is_total_internal_reflection(refraction_coeff) {
            reflected_colour
        } else {
            let transmitted_ray = compute_transmission_direction(
                hit.point,
                to_camera,
                hit.normal,
                eta,
                refraction_coeff.sqrt(),
                cos_incident_angle,
            );
            let transmission_colour = self.transmission_colour / (eta * eta);
            let transmitted_colour = scene.trace_ray(transmitted_ray, hit.depth);

            transmission_colour * transmitted_colour + self.reflection_colour * reflected_colour
        };

        luminance + reflected
    }
}

fn compute_transmission_direction(
    hit_point: Point3,
    to_camera: NormalVector3,
    normal: NormalVector3,
    eta: f32,
    cos_transmitted_angle: f32,
    cos_incident_angle: f32,
) -> Ray {
    let (normal, cos_incident_angle) = if cos_incident_angle >= 0.0 {
        (normal, cos_incident_angle)
    } else {
        (-normal, -cos_incident_angle)
    };
    let direction = -to_camera.0 / eta -
        normal.0 * (cos_transmitted_angle - cos_incident_angle / eta);
    Ray::new(hit_point, direction.normalise())
}

fn is_total_internal_reflection(refraction_coeff: f32) -> bool {
    refraction_coeff < 0.0
}

fn find_refraction_coeff(eta: f32, cos_incident_angle: f32) -> f32 {
    1.0 - (1.0 - cos_incident_angle * cos_incident_angle) / (eta * eta)
}

impl ReflectiveMaterial {
    fn shade_light(&self, hit: &HitInfo, light: &UniformLight, scene: &Scene) -> Colour {
        let luminance = self.phong.shade_light(hit, light, scene);

        let to_camera = -hit.ray.direction;
        let reflected_ray = Ray::new(hit.point, to_camera.reflect(hit.normal));
        let reflection = scene.trace_ray(reflected_ray, hit.depth) * self.reflection_colour;

        luminance + reflection
    }
}

impl DiffuseMaterial {
    fn shade_light(&self, hit: &HitInfo, light: &UniformLight, scene: &Scene) -> Colour {
        let source = light.sample();
        let in_direction = NormalVector3::direction(hit.point, source);
        let lambert_factor = in_direction.dot(hit.normal);
        if lambert_factor <= 0.0 {
            return Colour::black();
        }
        if scene.any_obstacle_between(hit.point, source) {
            return Colour::black();
        }
        light.colour * self.diffuse * lambert_factor
    }
}

impl Material for PhongMaterial {
    fn shade(&self, hit: &HitInfo, scene: &Scene) -> Colour {
        let mut total = self.ambient;
        for light in scene.lights.iter() {
            total = total + self.shade_light(hit, light, scene);
        }
        total
    }
}

impl PhongMaterial {
    fn shade_light(&self, hit: &HitInfo, light: &UniformLight, scene: &Scene) -> Colour {
        let source = light.sample();
        let in_direction = NormalVector3::direction(hit.point, source);
        let lambert_factor = in_direction.dot(hit.normal);
        if lambert_factor <= 0.0 {
            return Colour::black();
        }
        if scene.any_obstacle_between(hit.point, source) {
            return Colour::black();
        }
        let diffuse = light.colour * self.diffuse * lambert_factor;
        let phong_factor = PhongMaterial::phong_factor(
            in_direction,
            hit.normal,
            -hit.ray.direction,
            self.exponent,
        );
        diffuse + self.specular * phong_factor
    }

    fn phong_factor(
        in_direction: NormalVector3,
        normal: NormalVector3,
        to_camera: NormalVector3,
        specular_exponent: f32,
    ) -> f32 {
        let reflected = in_direction.reflect(normal);
        let cos_angle = reflected.dot(to_camera);
        if cos_angle <= 0.0 {
            0.0
        } else {
            cos_angle.powf(specular_exponent)
        }
    }
}

struct VisibleObject {
    shape: Box<Shape>,
    material: Box<Material>,
}

struct Scene {
    elements: Vec<VisibleObject>,
    lights: Vec<UniformLight>,
    background: Colour,
}

struct PinholeCamera {
    basis: OrthonormalBasis,
    eye: Point3,
    scale: f32,
    distance: f32,
}

impl PinholeCamera {
    fn new(eye: Point3, lookat: Point3, up: Vector3, scale: f32, distance: f32) -> PinholeCamera {
        PinholeCamera {
            basis: OrthonormalBasis::new(Vector3::direction(lookat, eye), up),
            eye: eye,
            scale: scale,
            distance: distance,
        }
    }

    fn get_ray(&self, x_frac: f32, y_frac: f32) -> Ray {
        let vec = Vector3::new(x_frac * self.scale, y_frac * self.scale, -self.distance);
        let direction = &self.basis * vec;
        Ray::new(self.eye, direction.normalise())
    }
}

const MAX_DEPTH: i32 = 5;

impl Scene {
    fn trace_ray(&self, ray: Ray, depth: i32) -> Colour {
        if depth >= MAX_DEPTH {
            return Colour::black();
        }
        let mut closest_ndx = None;
        let mut closest_info = IntersectionInfo {
            distance: std::f32::INFINITY,
            normal: Vector3::new(1.0, 0.0, 0.0).normalise(),
        };
        for (index, ref element) in self.elements.iter().enumerate() {
            let hit = element.shape.intersect(ray);
            match hit {
                Some(info) => {
                    if 0.0 < info.distance && info.distance < closest_info.distance {
                        closest_info = info;
                        closest_ndx = Some(index)
                    }
                }
                None => (),
            }
        }
        match closest_ndx {
            Some(index) => {

                let object = &self.elements[index];
                let hit = HitInfo::new(ray, closest_info, depth + 1);
                object.material.shade(&hit, self)
            }
            None => self.background,
        }
    }

    fn any_obstacle_between(&self, start: Point3, end: Point3) -> bool {
        self.any_obstacle_on_path(Ray::from_to(start, end), Vector3::distance(start, end))
    }

    fn any_obstacle_on_path(&self, ray: Ray, max_dist: f32) -> bool {
        for obj in self.elements.iter() {
            if let Some(hit) = obj.shape.intersect(ray) {
                if hit.distance > Ray::EPSILON && hit.distance < max_dist {
                    return true;
                }
            }
        }
        false
    }
}

fn to_rgb(colour: Colour) -> Rgb<u8> {
    let Colour(rgb) = colour;
    Rgb(
        [
            clamp(rgb.x * 256.0, 0.0, 255.0) as u8,
            clamp(rgb.y * 256.0, 0.0, 255.0) as u8,
            clamp(rgb.z * 256.0, 0.0, 255.0) as u8,
        ],
    )
}

fn raytrace(
    scene: &Scene,
    camera: &PinholeCamera,
    size: Size,
    antialias: &SampleSet2,
) -> ImageBuffer<Rgb<u8>, Vec<u8>> {
    let (width, height) = (size.x, size.y);
    ImageBuffer::from_fn(width, height, |x, y| {
        let mut result = Colour::black();
        for sample in antialias.samples.iter() {
            let x_frac = (((x as f32) + sample.x) / (width as f32)) * 2.0 - 1.0;
            let y_frac = (((y as f32) + sample.y) / (height as f32)) * 2.0 - 1.0;
            let ray = camera.get_ray(x_frac, -y_frac);
            result = result + scene.trace_ray(ray, 0)
        }
        to_rgb(result / antialias.samples.len() as f32)
    })
}

fn main() {
    let scene = Scene {
        elements: vec![
            VisibleObject {
                shape: Box::new(Sphere {
                    centre: Point3::new(4.0, 0.0, 0.0),
                    radius: 3.0,
                }),
                material: Box::new(TransparentMaterial {
                    phong: PhongMaterial {
                        ambient: Colour::rgb(0.0, 0.0, 0.0),
                        diffuse: Colour::rgb(0.3, 0.5, 0.3),
                        specular: Colour::rgb(0.3, 0.9, 0.3),
                        exponent: 100.0,
                    },
                    reflection_colour: Colour::rgb(0.4, 0.4, 0.9),
                    transmission_colour: Colour::rgb(0.9, 0.4, 0.4),
                    refraction: 0.8,
                }),
            },
            VisibleObject {
                shape: Box::new(Sphere {
                    centre: Point3::new(-4.0, 0.0, 0.0),
                    radius: 3.0,
                }),
                material: Box::new(TransparentMaterial {
                    phong: PhongMaterial {
                        ambient: Colour::rgb(0.0, 0.0, 0.0),
                        diffuse: Colour::rgb(0.3, 0.3, 0.5),
                        specular: Colour::rgb(0.9, 0.3, 0.3),
                        exponent: 100.0,
                    },
                    reflection_colour: Colour::rgb(0.9, 0.4, 0.4),
                    transmission_colour: Colour::rgb(0.3, 0.9, 0.3),
                    refraction: 0.8,
                }),
            },
            VisibleObject {
                shape: Box::new(Sphere {
                    centre: Point3::new(0.0, 0.0, 4.0),
                    radius: 3.0,
                }),
                material: Box::new(TransparentMaterial {
                    phong: PhongMaterial {
                        ambient: Colour::rgb(0.0, 0.0, 0.0),
                        diffuse: Colour::rgb(0.5, 0.3, 0.3),
                        specular: Colour::rgb(0.3, 0.3, 0.9),
                        exponent: 100.0,
                    },
                    reflection_colour: Colour::rgb(0.3, 0.9, 0.3),
                    transmission_colour: Colour::rgb(0.3, 0.3, 0.9),
                    refraction: 0.8,
                }),
            },
            VisibleObject {
                shape: Box::new(Plane {
                    point: Point3::new(0.0, 3.0, 0.0),
                    normal: Vector3::new(0.0, -1.0, 0.0).normalise(),
                }),
                material: Box::new(ReflectiveMaterial {
                    phong: PhongMaterial {
                        ambient: Colour::rgb(0.01, 0.1, 0.1),
                        diffuse: Colour::rgb(0.7, 0.7, 0.7),
                        specular: Colour::rgb(0.7, 0.7, 0.7),
                        exponent: 100.0,
                    },
                    reflection_colour: Colour::rgb(0.4, 0.4, 0.4),
                }),
            },
        ],
        lights: vec![
            UniformLight::point(Point3::new(5.0, -10.0, -5.0), Colour::white()),
        ],
        background: Colour::white() * 0.05,
    };

    let camera = PinholeCamera::new(
        Point3::new(0.0, -5.0, -20.0),
        Point3::new(0.0, 0.0, 0.0),
        Vector3::new(0.0, -1.0, 0.0),
        0.5,
        2.2,
    );

    let antialias = SampleSet2::regular(1);

    let image = raytrace(&scene, &camera, Size::new(500, 500), &antialias);
    image.save("output.png").unwrap();
}
